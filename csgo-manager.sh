#!/bin/bash
################################################################################
# Counter-Strike:Global Offensive Server Manager
####
# Provides easy management of a CS:GO server
#
# Author: Walt Elam
#
# Exit Codes:
#   $EXIT_OK    Everything executed successfully
#   $EXIT_ERROR Problem with configuration, or small issue
#   $EXIT_FAIL  Big issue, such as failing to install or missing files
#
# This is based off of the Linux Game Server scripts by Daniel Gibbs, found here
# https://github.com/dgibbs64/linuxgameservers
################################################################################

## Variables
############
# Configurable

# Do not touch
long_name="Counter-Strike:Global Offensive"
short_name="csgo"
op_mode=""

################################################################################

################################################################################
# check_for_lib
####
# Checks to make sure we have the main library  functions available
################################################################################
check_for_lib()
{
    lib_name="sdss_lib.sh"
    if [ ! -e "${lib_name}" ]; then
        lib_link="http://bitbucket.org/wrelam/"
        lib_link+="steam-dedicated-server-scripts/raw/master/${lib_name}"
        wget --no-check-certificate $lib_link

        if [ ! -e "${lib_name}" ]; then
            echo "Failed to retrieve library from ${lib_link}, exiting."
            echo "Download the ${lib_name} script and "
            echo "place it in this directory then retry."
            exit 1
        fi
    fi

    source ./${lib_name}

    print_ok "SDSS Library imported"
}

################################################################################
# usage
####
# Prints the usage menu
################################################################################
usage()
{
    cat << EOF
usage: manage a $long_name server
  $0 <mode> [OPTIONS...]

  Display mode specific help:
  $0 <mode> -h

  Modes
    install         Installs a $short_name server
    start           Start the $short_name server
    stop            Stop the $short_name server
    restart         Restart the $short_name server
    status          Check the status of a running $short_name server

  Options
    -h              Display this help menu
    -v              Display verbose output

EOF
}

################################################################################
# usage
####
# Prints help menu specific to each operation mode
################################################################################
mode_usage()
{
    case "${op_mode}" in
    "install")
        echo "usage: installs a $short_name server"
        echo "  $0 install"
        exit $EXIT_OK
        ;;
    *)
        usage
        print_fail  "Invalid operation mode"
        ;;
    esac
}

################################################################################
# parse_args
####
# Parses command-line arguments to setup for the script to run
################################################################################
parse_args()
{
    # Operation mode will always be the first parameter, any extras come later
    if  [[ $# -lt 1 ]]; then
        usage
        exit $EXIT_ERROR
    else
        op_mode=$1
        # Skip over the mode argument
        OPTIND=2

        if [[ "-h" == "${op_mode}" ]]; then
            usage
            exit $EXIT_OK
        fi
    fi

    while getopts "h" opt; do
        case "${opt}" in
        h)
            if [[ "" == "${op_mode}" ]]; then
                usage
                exit $EXIT_OK
            else
                mode_usage
                exit $EXIT_OK
            fi
            ;;
        :)
            print_fail "Option $OPTARG requires an argument" >&2
            exit $EXIT_ERROR
            ;;
        \?)
            print_fail "Invalid option: $OPTARG" >&2
            exit $EXIT_ERROR
            ;;
        esac
    done
}

################################################################################
# install_server
####
# Installs the server
################################################################################
install_server()
{
    print_ok "Server installed successfully"
}

## Main Script
##############
parse_args $@
check_for_lib
case "${op_mode}" in
"install")
    install_server
    ;;
*)
    usage
    print_fail "Invalid operation mode"
    exit $EXIT_ERROR
esac

exit $EXIT_OK
    
