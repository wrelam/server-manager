#!/bin/bash
################################################################################
# Steam Dedicated Server Scripts Library
####
# Library of common functions used across multiple server scripts.
#
# Author: Walt Elam
#
# This is based off of the Linux Game Server scripts by Daniel Gibbs, found here
# https://github.com/dgibbs64/linuxgameservers
################################################################################
# Paste the following code to the top of each script to ensure this library is
# included before running (and uncomment it):
#
# # Include the script library
# lib_name="sdss_lib.sh"
# if [ ! -e "${lib_name}" ]; then
#     lib_link="http://bitbucket.org/wrelam/"
#     lib_link+="steam-dedicated-server-scripts/raw/master/${lib_name}"
#     wget --no-check-certificate $lib_link
# 
#     if [ ! -e "${lib_name}" ]; then
#         echo "Failed to retrieve library from ${lib_link}, exiting."
#         echo "Download the ${lib_name} script and "
#         echo "place it in this directory then retry."
#         exit 1
#     fi
# fi
# 
# source ./${lib_name}
# 
# print_ok "SDSS Library imported"
# 
################################################################################

################################################################################
#   print_fail()
####
#   Prints a message prepended with [ FAIL ]
################################################################################
print_fail()
{
    echo -e "\r\033[K[\e[0;31m FAIL \e[0;39m] $@"
}

################################################################################
#   print_ok()
####
#   Prints a message prepended with [  OK  ]
################################################################################
print_ok()
{
    echo -e "\r\033[K[\e[0;32m  OK  \e[0;39m] $@"
}

################################################################################
#   print_info()
####
#   Prints a message prepended with [ INFO ]
################################################################################
print_info()
{
    echo -e "\r\033[K[\e[0;36m INFO \e[0;39m] $@"
}

